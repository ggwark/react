import React, {useEffect} from 'react';
import Context from '../Context';
import NewsList from './NewsList';
import AddNews from './AddNews';

export default function News () {

  const [news, setNews] = React.useState([]);

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/posts?_limit=10')
    .then(response => response.json())
    .then(json => {
      setTimeout(() => {
        setNews(
          json.map(post => {
            post['countReaded'] = post.id * 10;
            post['visible'] = false;
            post['read'] = false;
    
            return post;
          })
        )
      }, 1000);
    })
  }, []);


  let readChange = (id) => {
    setNews(news.map(item => {
      if(item.id === id) {
        item.read = !item.read;
      }
      return item;
    }));    
  }

  function removeNews (id) {
    setNews(news.filter(item => item.id !== id))
  }



  function createNews(title) {
    setNews(news.concat([
      {id: Date.now(), countReaded: 0, title, visible: false, read: false}
    ]));    
  }

  return (
    <Context.Provider value={{removeNews}}>

    <div className="container">
    <h2>
      lesson react
    </h2>
    <AddNews onCreateNews={createNews}/>
    <NewsList news={news} changeRead={readChange}/>
    {news.length === 0 && (
      <p>новин більше немає</p>
    )}
  </div>
  </Context.Provider>

  )
}

