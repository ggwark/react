import React, {useState} from 'react';

function useInputTitle (defaultValue = '') {
  const [value, setValue] = useState(defaultValue);

  return {
    bind: {
      value,
      onChange: event => setValue(event.target.value)
    },
    value: () => value,
    clear: () => setValue('')
  };
}



export default function AddNews ({onCreateNews}) {

  const inputTitle = useInputTitle('hello');
  

  function submitData (event) {
    event.preventDefault();
    if(inputTitle.value().trim()) {
      onCreateNews(inputTitle.value());
      inputTitle.clear();
    }
  }

  return (
    <form onSubmit={submitData}>
      <input {...inputTitle.bind}></input>
      <input type="submit"></input>
    </form>
  )
}