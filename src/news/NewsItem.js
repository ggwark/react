import React, {useContext} from 'react';
import Context from '../Context';

export default function NewsItem ({ news, readChange }) {

  const {removeNews} = useContext(Context);

  let classesForMain = ['card'];
  if(news.read) {
    classesForMain.push('read');
  }

  return (
      <div className={classesForMain.join(' ')}>
        <img src="https://images.theconversation.com/files/336248/original/file-20200520-152302-97x8pw.jpg?ixlib=rb-1.1.0&rect=0%2C1248%2C6837%2C3418&q=45&auto=format&w=1356&h=668&fit=crop" alt="Avatar"/>
        <div className="text">
          <h4><b>{news.title}</b></h4> 
          <button onClick={()=> removeNews(news.id)}>close</button>
          <input id={'is-readed' + news.id} type="checkbox" onChange={() => readChange(news.id)} checked={news.read}></input>
          <label htmlFor={'is-readed' + news.id}>Прочитано</label>
        </div>
      </div>)
} 