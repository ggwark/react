import React from 'react';
import Header from './layout/Header';
import Home from './home/Home';
import News from './news/News';

export default function App() {



  return (
    <>
      <Header/>

      <Home/>
      <News/>
    </>
  );
}
