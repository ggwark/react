import React from 'react';
import './Header.css';

export default function Header () {

  return (
    <header>
      <ul>
        <li>HOME</li>
        <li>NEWS</li>
      </ul>
    </header>
  )
}

